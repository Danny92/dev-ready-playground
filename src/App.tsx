import React from 'react';
import logo from './logo.svg';
import './App.css';
import NormalShowcase from './Showcases/Normal_showcase/NormalShowcase';

function App() {
  return (
    <NormalShowcase/>
  );
}

export default App;
