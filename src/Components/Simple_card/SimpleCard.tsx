import React from "react";

interface ContainerProps {
  content: any;
  background?: string;
  border_radius?: number;
  width?: number | string;
  content_padding?: number | string;
  maring?: string | string;
  box_shadow?: string;
}

const SimpleCard: React.FC<ContainerProps> = (props) => {
  const styles = {
    card: {
      width: props.width || "90%",
      margin: props.maring || "10px auto",
      borderRadius: props.border_radius || 5,
      boxShadow: props.box_shadow || "0 0 3px 1px rgb(139, 135, 135)",
      background: props.background || "#f1f2f3",
    },
    content: {
        padding: props.content_padding || "4px 7px",
    }
  };
  return <div style={styles.card}>
      <div style={styles.content}>{props.content}
      </div></div>;
};

export default SimpleCard;
