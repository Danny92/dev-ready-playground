import React from "react";

interface ContainerProps {
  content: any;
  card_background?: string;
  card_border_radius?: number;
  card_width?: number | string;
  card_maring?: string | string;
  card_box_shadow?: string;
  header_background?: string;
  header_padding?: number | string;
  content_padding?: number | string;
}

const CardWithHeader: React.FC<ContainerProps> = (props) => {
  const styles = {
    card: {
      width: props.card_width || "90%",
      margin: props.card_maring || "10px auto",
      borderRadius: props.card_border_radius || 5,
      boxShadow: props.card_box_shadow || "0 0 3px 1px rgb(139, 135, 135)",
      background: props.card_background || "#f1f2f3",
      overflow: props.header_background || "hidden",
    },
    header: {
        padding: props.header_padding || "4px 7px",
        background: props.header_background || "rgb(184, 182, 182)"
    },
    content: {
        padding: props.content_padding || "4px 7px",
    },
  };
  return (
    <div style={styles.card}>
      <div style={styles.header}>header</div>
      <div style={styles.content}>{props.content}</div>
    </div>
  );
};

export default CardWithHeader;
