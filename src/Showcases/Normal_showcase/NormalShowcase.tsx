import React from "react";
import "./NormalShowcase.css";
import SimpleCard from "../../Components/Simple_card/SimpleCard";
import CardWithHeader from "../../Components/Card_with_header/CardWithHeader";

interface ContainerProps {
}

const NormalShowcase: React.FC<ContainerProps> = (props) => {
  return (
    <div className="normal-showcase-root">
      <SimpleCard content={"aaa"} />
      <CardWithHeader content={"aaaaaa"}/>
    </div>
  );
};

export default NormalShowcase;
